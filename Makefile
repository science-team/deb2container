# Makefile to install deb2container.
# required: bash
#
# just type: sudo make install

all:
	# nothing to do. Please use: sudo make install

install:
	install -D bin/deb2docker    $(DESTDIR)$(prefix)/usr/bin/deb2docker
	install -D bin/deb2apptainer $(DESTDIR)$(prefix)/usr/bin/deb2apptainer
	install -D man/deb2apptainer.1 $(DESTDIR)$(prefix)/usr/share/man/man1/deb2apptainer.1
	install -D man/deb2docker.1    $(DESTDIR)$(prefix)/usr/share/man/man1/deb2docker.1

uninstall:
	-rm -f $(DESTDIR)$(prefix)/usr/bin/deb2docker
	-rm -f $(DESTDIR)$(prefix)/usr/bin/deb2apptainer
	-rm -f $(DESTDIR)$(prefix)/usr/share/man/man1/deb2apptainer.1
	-rm -f $(DESTDIR)$(prefix)/usr/share/man/man1/deb2docker.1
	
deb:
	cp -r packaging/debian .
	debuild -b || true
	-rm -rf debian

.PHONY: install uninstall deb

