% DEB2DOCKER(1)
% Emmanuel Farhi
% November 2024

# NAME

**deb2docker** - Build a Docker image with given Debian packages

# SYNOPSIS

**deb2docker** [-hB][-c CMD][-f FROM][-i FILE][-n NAME][-o DIR][-p PRE_SCRIPT][-s POST_SCRIPT] *packages*

# DESCRIPTION

**deb2docker** is a simple script which takes as input a list of Debian packages
and generates automatically a Docker container including these packages. A set
of *freedesktop.org* desktop launchers are also generated based on the .desktop 
and icon files found in the packages. In addition, a desktop launcher is 
created to start the container in a Terminal.

This tool is suited for deploying applications as containers, as well as for
testing Debian packages in a sandbox.

See examples below to tune old Debian releases.
  
# OPTIONS

**-B**
:   do NOT build the image (default is to build). 
    A `build` script is generated in the DIR target directory.

**-c EXEC**
:   Command to run in the container (default to `/bin/bash`).

**-f FROM**
:   Distribution is to be used (default to `debian:stable`).

**-h**
:   Show this help

**-i FILE**
:   Include/copy file/dir (to /opt/install/files in target). 
    Multiple options are possible.

**-n NAME**
:   Name of the image (default is built from the package list).

**-o DIR**
:   Use given directory DIR for the build (default is in /tmp).

**-p PRE_SCRIPT**
:   Execute the given script `PRE_SCRIPT` before packages install.

**-s POST_SCRIPT**
:   Execute the given script `POST_SCRIPT` after packages install.

**packages**
:   The package list can be any Debian package, as well as local .deb files.

# FILES

- DIR/README
- DIR/Dockerfile
- DIR/launchers/
- DIR/icons/
- DIR/build
- DIR/start

# NOTES

You need of course to have Docker installed and be part of the `docker` group:
:   - sudo apt install docker.io
    - sudo usermod -aG docker $USER`

You may have to manually configure Docker to pass a proxy configuration.

Usual commands typically used to handle Docker containers are:

**build**
:   docker build --rm Dockerfile
**run**
:   docker run   --rm -it NAME
**clean**
:   docker rmi NAME
**clean ALL**
:   docker system prune -a

# EXAMPLES

Create a Docker container with package `x11-apps` in directory `/tmp/xeyes`, and launch `xeyes`:
:   - deb2docker -o /tmp/xeyes x11-apps
    - /tmp/xeyes/start xeyes
    
    A Desktop launcher is created as /tmp/xeyes/launchers/x11-apps-terminal.desktop

Create a Docker container with `x11-apps` and `meshlab`
:   deb2docker x11-apps meshlab

Create a Docker container making sure software channels are active:
:   - echo "sed -i 's/main/main contrib non-free/g' /etc/apt/sources.list" > pre.sh
    - deb2docker -p pre.sh x11-apps

Create a Docker container based on specific Debian version, and make sure sources are right:
:   - echo "echo 'deb http://deb.debian.org/debian bullseye main contrib non-free' >> /etc/apt/sources.list" > pre-script.sh
    - deb2docker -f debian:bullseye -p pre-script.sh x11-apps

# AUTHORS

Emmanuel Farhi (emmanuel.farhi@synchrotron-soleil.fr)

# SEE ALSO

deb2apptainer(1), distrobox-create(1), distrobox-enter(1), docker(1), apptainer(1)

