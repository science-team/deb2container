% DEB2APPTAINER(1)
% Emmanuel Farhi
% November 2024

# NAME

**deb2apptainer** - Build a Singularity/Apptainer image with given Debian packages

# SYNOPSIS

**deb2apptainer** [-hB][-c CMD][-f FROM][-i FILE][-n NAME][-o DIR][-p PRE_SCRIPT][-s POST_SCRIPT] *packages* 

# DESCRIPTION

**deb2apptainer** is a simple script which takes as input a list of Debian packages
and generates automatically a Singularity/Apptainer container including these packages. A set
of *freedesktop.org* desktop launchers are also generated based on the .desktop 
and icon files found in the packages. In addition, a desktop launcher is 
created to start the container in a Terminal.

This tool is suited for deploying applications as containers, as well as for
testing Debian packages in a sandbox.

See examples below to tune old Debian releases or make use of attached GPUs.
  
# OPTIONS

**-B**
:   do NOT build the image (default is to build). 
    A `build` script is generated in the DIR target directory.

**-c EXEC**
:   Command to run in the container (default to `/bin/bash`).

**-f FROM**
:   Distribution is to be used (default to `debian:stable`).

**-h**
:   Show this help

**-i FILE**
:   Include/copy file/dir (to /opt/install/files in target). 
    Multiple options are possible.

**-n NAME**
:   Name of the image (default is built from the package list).

**-o DIR**
:   Use given directory DIR for the build (default is in /tmp).

**-p PRE_SCRIPT**
:   Execute the given script `PRE_SCRIPT` before packages install.

**-s POST_SCRIPT**
:   Execute the given script `POST_SCRIPT` after packages install.

**packages** 
:   The package list can be any Debian package, as well as local .deb files.

# FILES

- DIR/README
- DIR/image.def
- DIR/image.sif
- DIR/launchers/
- DIR/icons/
- DIR/build
- DIR/start

# NOTES

You obviously require to have `apptainer` installed.

Get the Debian package at:
- https://apptainer.org/docs/admin/main/installation.html#install-debian-packages

In case you get an error such as:
```
E: setgroups 65534 failed - setgroups (1: Operation not permitted)
E: setegid 65534 failed - setegid (22: Invalid argument)
E: seteuid 100 failed - seteuid (22: Invalid argument)
E: setgroups 0 failed - setgroups (1: Operation not permitted)
```
then you should execute the `deb2apptainer` command as administrator (sudo).
An other lighter solution is to set the files `/etc/subuid` and `/etc/subgid` (same content) as:
```
$USER:100000:65536
```
where you should replace `$USER` with your actual user name.

Usual commands typically used to handle Apptainer/Singularity containers are:

**build**
:     apptainer build      image.sif

**run**
:     apptainer run        image.sif
      apptainer run  --nv  image.sif # with NVIDIA GPU pass-through

**info**
:     apptainer inspect    image.sif

**header**
:     apptainer sif header image.sif

**data**
:     apptainer sif list   image.sif

# EXAMPLES

Create a Singularity/Apptainer container with package `x11-apps` in directory `/tmp/xeyes`, and launch `xeyes`:
:   - deb2apptainer -o /tmp/xeyes x11-apps
    - /tmp/xeyes/start xeyes
    
    A Desktop launcher is created as /tmp/xeyes/launchers/x11-apps-terminal.desktop

Create a Singularity/Apptainer container with `x11-apps` and `meshlab`
:   deb2apptainer x11-apps meshlab

Create a Singularity/Apptainer container making sure software channels are active:
:   - echo "sed -i 's/main/main contrib non-free/g' /etc/apt/sources.list" > pre.sh
    - deb2apptainer -p pre.sh x11-apps
    
Create a Singularity/Apptainer container based on specific Debian version, and make use of the GPU:
:   - echo "echo 'deb http://deb.debian.org/debian bullseye main contrib non-free' >> /etc/apt/sources.list" > pre-script.sh
    - deb2apptainer -n pyhst2 -f debian:bullseye -p pre-script.sh -o /tmp/apptainer-pyhst2/ python3-pyhst2-cuda nvidia-smi nvidia-cuda-toolkit
    - apptainer run --nv /tmp/apptainer-pyhst2/pyhst2.sif nvidia-smi

# AUTHORS

Emmanuel Farhi (emmanuel.farhi@synchrotron-soleil.fr)

# SEE ALSO

deb2docker(1), distrobox-create(1), distrobox-enter(1), docker(1), apptainer(1)

